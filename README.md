## KPCLib - KeePass Portable Class Library

This is a port of KeePassLib to PCL and .netstandard so that it can be used to build with Xamarin.Forms. With KPCLib and Xamarin.Forms, we can build KeePass based applications on all major platforms.


### Setup
* Available on NuGet: https://www.nuget.org/packages/KPCLib [![NuGet](https://img.shields.io/nuget/v/Xam.Plugin.Media.svg?label=NuGet)](https://www.nuget.org/packages/KPCLib/)
* Install into your PCL/.NET Standard project and Client projects.


